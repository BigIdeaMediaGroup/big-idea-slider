## About
Web components images slider. Built using Poylmer.

##### Dependencies
    Poylmer 1.2.0
## Setup
#### Clone
``` shell
    $ git clone https://gitlab.com/BigIdeaMediaGroup/big-idea-slider.git
```
#### Run
``` bower install ```

#### Examples
``` html
<!DOCTYPE html>
<html>
    <head>
        <script src="bower_components/webcomponentsjs/webcomponents-lite.js"></script>
        <link rel="import" href="slider-elements.html">
    </head>
    <body>
        <image-slider speed=2000 delay=3000 scaling=100>
            <image-slide path="/demo-data/winter.jpg" alt="hello"></image-slide>
            <image-slide path="/demo-data/location.jpg"></image-slide>
            <image-slide path="/demo-data/promise.jpg"></image-slide>
        </image-slider>
    </body>
</html>
```

#### Options
##### image-slider
    'speed' : transition time in ms
    'delay' : time between slide transitions in ms
    'scaling' : wait time before resizing after window resize

##### image-slide
    'path' : image path
    'alt' : image alt tag

## Testing Server
Useful for testing on your local machine.
#### Install
``` shell
    $ npm install http-server -g
```



#### Run
``` shell
    $ http-server
```
